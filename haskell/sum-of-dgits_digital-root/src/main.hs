module DigitalRoot where

digitalRoot :: Integral a => a -> a
digitalRoot 0 = 0
digitalRoot n = 1 + (n - 1) `mod` 9
