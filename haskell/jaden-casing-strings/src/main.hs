module JadenCasing where

import qualified Data.Char as Char

toJadenCase :: String -> String
toJadenCase s = init $ foldr (\r e -> r ++ " " ++ e) "" (map capitalized (words s))
  where
    capitalized (head:tail) =
      Char.toUpper head : map Char.toLower tail