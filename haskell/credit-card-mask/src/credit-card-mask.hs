module Maskify where

slice :: Int -> Int -> String -> String 
slice from to xs = take (to - from + 1) (drop from xs)

maskify :: String -> String
maskify str =
  (map (\x -> '#') (slice 0 (length str - 5) str))
  ++
  (slice (length str - 4) (length str) str) 