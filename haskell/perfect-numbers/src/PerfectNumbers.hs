module PerfectNumbers (classify, Classification(..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

aliquotSum :: Int -> Int
aliquotSum n = sum (acc n 1 [])
  where
    acc :: Int -> Int -> [Int] -> [Int]
    acc n i res =
      if n `mod` i == 0 && i < n
      then acc n (i + 1) res:n
      else res

classify :: Int -> Maybe Classification
classify n
  | (aliquotSum n) == n = Perfect
  | (aliquotSum n) > n  = Abundant
  | (aliquotSum n) < n  = Deficient