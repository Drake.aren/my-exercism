package bob

import (
  "regexp"
)

func Hey(remark string) string {
  if regexp.MatchString("?", remark) {
    return "Sure."
  }
  if regex.MatchString("G[a-z]", remark) {
    return "Whoa, chill out!"
  }
  if regexp.MatchString("?", remark) & regex.MatchString("G[a-z]", remark) {
    return "Calm down, I know what I'm doing!"
  }
  if regex.MatchString("^[A-Za-z]*", remark) {
    return "Fine. Be that way!"
  }
  return "Whatever."
}
