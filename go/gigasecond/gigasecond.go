package gigasecond

import "time"

func Pow(a, b int) int {
  p := 1
  for b > 0 {
    if b&1 != 0 { p *= a }
    b >>= 1
    a *= a
  }
  return p
}

func AddGigasecond(t time.Time) time.Time {
  return t.Add(time.Second * time.Duration((Pow(10,9))))
}
